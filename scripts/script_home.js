"use strict";
$(document).ready(function() {
	var isLowercase = true;
	var showLastfm = true;
	var updateFreq = 60;
	var interval;
	function encode(x) {
		return $('<div>').text(x).html();
	}
	function requestLastFm() {
		$('.song').html('<span class="loading">Loading data...</span>');
		$('#musicrefresh .fa-refresh').addClass("fa-spin");
		$.getJSON("https://ws.audioscrobbler.com/2.0/?method=user.getRecentTracks&user=serin113&api_key=1ef77c2d4d7a855c35d4875740fd628b&limit=1&format=json&callback=?", function(data) {
			var html = '';
			var counter = 1;
			$.each(data.recenttracks.track, function(i, item) {
				var songTitle = encode(item.name);
				var songArtist = encode(item.artist['#text']);
				var songAlbum = encode(item.album['#text']);
				if(counter == 1) {
					html += '<a href="' + item.url + '" target="_blank">' + songTitle + '<span class="artist"> <i><small>by</small> ' + songArtist + ' <small>in</small> ' + songAlbum + '</i></span></a>';
					$('#musicrefresh .fa-refresh').removeClass("fa-spin");
					$('.song').html(html);
				}
				counter++;
			});
		}).fail(function() {
			$('.song').html('<span class="loading">Failed to load data</span>');
		});
	}
	requestLastFm();
	interval = setInterval(requestLastFm, updateFreq*1000);
	$('#caseToggle').click(function() {
		if (isLowercase) {
			$('.toggleicon-case').removeClass("fa-toggle-on");
			$('.toggleicon-case').addClass("fa-toggle-off");
			$("body").css("text-transform", "none");
		} else {
			$('.toggleicon-case').removeClass("fa-toggle-off");
			$('.toggleicon-case').addClass("fa-toggle-on");
			$("body").css("text-transform", "lowercase");
		}
		isLowercase = !isLowercase;
	});
	$('#lastfmToggle').click(function() {
		if (showLastfm) {
			$('.toggleicon-lastfm').removeClass("fa-toggle-on");
			$('.toggleicon-lastfm').addClass("fa-toggle-off");
			$('#nowplaying').css("display", "none");
			clearInterval(interval);
		} else {
			$('.toggleicon-lastfm').removeClass("fa-toggle-off");
			$('.toggleicon-lastfm').addClass("fa-toggle-on");
			$('#nowplaying').css("display", "block");
			interval = setInterval(requestLastFm, 30*1000);
		}
		showLastfm = !showLastfm;
	});
	$('#musicrefresh').click(function() {
		clearInterval(interval);
		requestLastFm();
		interval = setInterval(requestLastFm, 30*1000);
	});
});